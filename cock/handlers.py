# coding: utf-8

import bottle

def static_file(path):
	def callback(file):
		print(path + file)
		return bottle.static_file(file, root=path)
	return callback

