# coding: utf-8

import jinja2
from .utils import current_app

def reset_app(app):
	app.config["_view"] = {}
	loader = None

	if "module" in app.config:
		loader = jinja2.PackageLoader(app.config["module"], "views")
	elif "views_path" in app.config:
		loader = jinja2.FileSystemLoader(app.config["views_path"])
	else:
		loader = jinja2.FileSystemLoader("views/")

	env = jinja2.Environment(loader=loader)
	env.globals["app"] = app
	env.globals["config"] = app.config

	app.config["_view"]["loader"] = loader
	app.config["_view"]["env"] = env
	app.config["_view"]["elems"] = {}

	return app

def render(name, **kwargs):
	app = current_app()
	try:
		template = app.config["_view"]["env"].get_template(name)
	except jinja2.TemplateNotFound:
		template = app.config["_view"]["env"].get_template(name + ".html")

	return template.render(kwargs)

