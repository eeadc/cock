# coding: utf-8

import bottle

def current_app():
	return bottle.request.environ["bottle.app"]

def generate_hash():
	import hashlib, hmac, random

	digest = hashlib.sha256()
	digest.update(bytes(random.randrange(0, 2**8)))
	return digest.hexdigest().encode("utf-8")
