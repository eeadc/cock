# coding: utf-8

import hashlib
import bottle

from .utils import current_app, generate_hash

# The Session class provides simple access to stateful data. You have to configure your
# application with some parameters to have access to session data:
#
#   session.cookie:  The Session Cookie
#   session.save:    A method to save the session in the backend. It takes the parameters
#                    'app', 'id', and 'data'
#   session.destroy: A method to destroy the session. It takes the parameters
#                    'app' and 'id'
#   session.get:     A method to get the session data from the backend.
#                    It takes the parameters 'app' and 'id'
#
class Session:
	@classmethod
	def current(cls):
		if "cock.session" in bottle.request.environ:
			return bottle.request.environ["cock.session"]

		return cls()

	def __init__(self, app=None):
		self.id = None
		self.app = app or current_app()

		self.has_cookie = False
		if "cock.session" in bottle.request.environ:
			self.id = bottle.request.environ["cock.session"].id
			self.has_cookie = bottle.request.environ("cock.session").has_cookie
		elif self.app.config["session.cookie"] in bottle.request.cookies:
			self.id = bottle.request.cookies[self.app.config["session.cookie"]]
			self.has_cookie = True
		else:
			self.id = generate_hash()

		if "cock.session" not in bottle.request.environ:
			bottle.request.environ["cock.session"] = self

	def __getitem__(self, name):
		return get_data()[name]

	def __setitem__(self, name, value):
		data = get_data()
		data[name] = value

		if not self.has_cookie:
			create_cookie()

		return self.save_data(data)

	def __delitem__(self, name):
		data = get_data()
		del data[name]

		return self.save_data(data)

	def __contains__(self, name):
		return name in self.get_data()

	def get(self, name, default=None):
		return self.get_data().get(name, default)

	def get_data(self):
		return self.app.config["session.get"](self.app, self.id) or {}

	def save_data(self, data):
		return self.app.config["session.save"](self.app, self.id, data)

	def create_cookie(self):
		bottle.response.set_cookie(self.app.config["session.cookie"], self.id)
		self.has_cookie = True

