# coding: utf-8

import bottle

import cock.view
import cock.handlers
import cock.utils

# The built-in cock modules
# ONLY CHANGE THIS IF YOU KNOW WHAT YOU ARE DOING
_builtin_modules = [ cock.view ]

# Initializes a cock application
#
# routes: List of tuples describing the routes. The tuples have the form
#         (method, path, action) where method is a valid HTTP/1.1 action,
#         path a Bottle routing path
#         (see http://bottlepy.org/docs/dev/tutorial.html#request-routing)
#         and action is a method.
# config: Application configuration for views etc.
#         Common configuration keys are e.g. "database" or "module"
#
# Returns a WSGI application
def cock(routes=[], config={}, modules=[], bottle=bottle):
	app = bottle.Bottle()
	app.config.update(config)

	for route in routes:
		if route[0] == "STATIC":
			app.route(route[1])(handlers.static_file(route[2]))
			continue
		if route[0] == "ERROR":
			app.error(route[1])(route[2])
			continue
		if route[0] == "MOUNT":
			app.mount(route[1], route[2])
			continue
		# Build a simple route name
		try:
			route_name = route[2].__module__.split('.')[-1] + "_" + route[2].__name__
		except AttributeError:
			route_name = route[2].__name__
		if route[0] == "ANY":
			app.route(route[1], name=route_name)(route[2])
		else:
			app.route(route[1], method=route[0], name=route_name)(route[2])
	
	for module in _builtin_modules:
		module.reset_app(app)
	for module in modules:
		module.reset_app(app)
	
	return app

render = view.render
current_app = utils.current_app

def session():
	import cock.session
	return session.Session.current()
